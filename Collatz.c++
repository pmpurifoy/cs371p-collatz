// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

#include "Collatz.h"

using namespace std;

// Pre-calculated values stored into integer array for quick access.
int metacache[100] =
{
    179, 279, 308, 324, 314, 340, 335, 351, 333, 333, 354, 349, 344, 344, 375,
    383, 370, 347, 365, 360, 373, 386, 368, 443, 368, 363, 407, 407, 389, 371,
    371, 384, 384, 366, 441, 379, 410, 423, 436, 405, 405, 449, 418, 400, 369,
    387, 444, 382, 413, 426, 426, 470, 408, 377, 452, 421, 421, 390, 434, 403,
    403, 447, 509, 416, 416, 429, 442, 385, 398, 442, 504, 411, 411, 424, 393,
    424, 468, 437, 406, 468, 406, 450, 450, 525, 419, 419, 388, 432, 445, 370,
    445, 476, 476, 507, 383, 414, 414, 458, 440
};


// The size of the range contained at each index.
const int INTERVAL_SIZE = 10000;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    assert(i > 0 && i < 1000000);
    assert(j > 0 && j < 1000000);

    int result = 0;

    int start = i;
    int end = j;

    // Need to swap the inputs if the lowerBound was greater then the upperBound
    if(start > end) {
        int temp = end;
        end = start;
        start = temp;
    }

    /*
      *** MCL optimization ***
      Let x be 1/2 * upper bound + 1

      If the lower bound is less than x, the max cycle length of (lowerBound, upperBound)
      is equal to the max cycle length of (x, upperBound).
    */
    while(start < (end / 2) + 1) {
        start = end / 2 + 1;
    }

    int startIndex = start / INTERVAL_SIZE;
    int endIndex = end / INTERVAL_SIZE;

    /* Chop off the front end and see if we're done or we need to keep going */
    if(startIndex == endIndex) {
        return get_mcl(start, end);
    } else {
        result = get_mcl(start, (startIndex + 1) * INTERVAL_SIZE - 1);
        startIndex++;
    }

    /*
       Use precomputed values in the metacache instead of calculating range
       until the start and the end are in the same range.
    */
    while(startIndex != endIndex) {
        if(result < metacache[startIndex]) {
            result = metacache[startIndex];
        }
        startIndex++;
    }

    /* The remainder of the lengths to be calculated can be done with get_mcl */
    int tail = get_mcl(startIndex * INTERVAL_SIZE, end);
    if(result < tail) {
        assert(tail > 0);
        return tail;
    } else {
        assert(result > 0);
        return result;
    }
}

/* Function used to precalculate ranges */

/*
void get_metacache() {
    cout << get_mcl(1, 1000) << ", ";
    for(int k = 1; k < 98; k++) {
        int lower = k * INTERVAL_SIZE + 1;
        int upper = (k + 1) * INTERVAL_SIZE - 1;
        cout << get_mcl(lower, upper) << ", " << endl;
    }
    cout << get_mcl(990000, 999999);
}
*/

// ------------
// get_mcl
// ------------

int get_mcl(int i, int j) {
    assert(i > 0 && i < 1000000);
    assert(j > 0 && j < 1000000);
    int start = i;
    int end = j;

    int max = 0;
    for (int begin = start; begin <= end; begin++) {
        assert(begin > 0);
        // Need to make num unsigned long because overflow.
        unsigned long num = begin;
        int count = 1;
        while(num > 1) {
            if (num % 2 == 1) {
                // Optimization added for odd numbers, does two steps in 1.
                num = num + (num >> 1) + 1;
                count++;
            } else {
                num = (num / 2);
            }
            count++;
        }
        if(count > max) {
            max = count;
        }
        assert(count > 0);
    }
    assert(max > 0);
    return max;
}
// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
